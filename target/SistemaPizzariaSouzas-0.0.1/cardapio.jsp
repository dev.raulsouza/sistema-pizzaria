<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="./resources/bootstrap/img/icon.png">

<title>Souza's Pizzaria</title>

<link rel="canonical"
	href="https://getbootstrap.com/docs/4.0/examples/album/">

<!-- Bootstrap core CSS -->
<link href="./resources/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./resources/bootstrap/css/album.css" rel="stylesheet">
</head>

<body>

	<header>
		<div class="navbar navbar-dark bg-dark box-shadow">
			<div class="container d-flex justify-content-between">
				<a href="index.jsp" class="navbar-brand d-flex align-items-center">
					<strong>Souza's Pizzaria</strong>
				</a>
				<p class="lead">
					<a href="pedido.jsp" class="btn btn-lg btn-secondary">Fa�a seu
						pedido!</a>
				</p>
			</div>
		</div>
	</header>

	<main role="main">

		<section class="jumbotron text-center">
			<div class="container">
				<h1 class="jumbotron-heading">Diversidade de sabores!</h1>
				<p class="lead text-muted">Pizzas salgadas, doces, diferentes
					tipos de massas, diferentes tipos de bordas...Venha conferir!</p>
			</div>
		</section>

		<div class="album py-5 bg-light">
			<div class="container">

				<div class="row">
					<div class="col-md-4">
						<div class="card mb-4 box-shadow">
							<img class="card-img-top"
								src="./resources/bootstrap/css/icon2.png" alt="" width="72"
								alt="Card image cap">
							<div class="card-body">
								<p class="card-text">Mussarela</p>
								<div class="d-flex justify-content-between align-items-center">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</main>

	<footer class="text-muted">
		<div class="container">
			<p class="float-right">
				<a href="#">Back to top</a>
			</p>
			<p>Album example is &copy; Bootstrap, but please download and
				customize it for yourself!</p>
			<p>
				New to Bootstrap? <a href="../../">Visit the homepage</a> or read
				our <a href="../../getting-started/">getting started guide</a>.
			</p>
		</div>
	</footer>

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
	</script>
	<script src="../../assets/js/vendor/popper.min.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="../../assets/js/vendor/holder.min.js"></script>
</body>
</html>