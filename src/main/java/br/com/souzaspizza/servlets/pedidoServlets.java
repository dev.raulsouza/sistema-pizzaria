package br.com.souzaspizza.servlets;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "pedido Servlets", urlPatterns = { "/pedido" })
public class pedidoServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public pedidoServlets() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			
			int numPedido = 1;
			
			String nome = request.getParameter("nome").toUpperCase();
			String sobrenome = request.getParameter("sobrenome").toUpperCase();
			String email = request.getParameter("email").toUpperCase();
			String endere�o = request.getParameter("endereco").toUpperCase();
			String cidade = request.getParameter("cidade").toUpperCase();
			String pref = request.getParameter("pref").toUpperCase();
			String cep = request.getParameter("cep").toUpperCase();
			String massa = request.getParameter("massa").toUpperCase();
			String borda = request.getParameter("borda").toUpperCase();
			String sabor1 = request.getParameter("sabor1").toUpperCase();
			String sabor2 = request.getParameter("sabor2").toUpperCase();
			String bebida = request.getParameter("bebidas").toUpperCase();
			String doce = request.getParameter("doce").toUpperCase();
			String obs = request.getParameter("obs").toUpperCase();
			String pagamento =  request.getParameter("pgmt").toUpperCase();
			
			FileWriter arq = new FileWriter("S:\\Documentos\\FACULDADE\\pedido_"+ numPedido + ".txt"); 
			PrintWriter gravarArq = new PrintWriter(arq);
			
			gravarArq.println("----- PEDIDO NOVO -----");
			gravarArq.println(" ");
			gravarArq.println("--------- DADOS ---------");
			gravarArq.println("Nome: " + nome + " " + sobrenome);
			gravarArq.println("Email: " + email);
			gravarArq.println("Endere�o: " + endere�o);
			gravarArq.println("Cidade: " + cidade);
			gravarArq.println("Ponto de refer�ncia: " + pref);
			gravarArq.println("CEP: " + cep);
			gravarArq.println(" ");
			gravarArq.println("-------- PEDIDO ---------");
			gravarArq.println("Massa: " + massa);
			gravarArq.println("Borda: " + borda);
			gravarArq.println("Sabor 1: " + sabor1);
			gravarArq.println("Sabor 2: " + sabor2);
			gravarArq.println("Bebida: " + bebida);
			gravarArq.println("Doce: " + doce);
			gravarArq.println(" ");
			gravarArq.println("----- PAGAMENTO -----");
			gravarArq.println(pagamento);
			gravarArq.println(" ");
			gravarArq.println("---- OBSERVA��ES ----");
			gravarArq.println(obs);
			
			
			arq.close();
			response.sendRedirect("fim.jsp");

		} catch (Exception e) {

		}

	}

}
