<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

<title>Pedido</title>

<link rel="canonical"
	href="https://getbootstrap.com/docs/4.0/examples/checkout/">

<!-- Bootstrap core CSS -->
<link href="./resources/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./resources/bootstrap/css/form-validation.css"
	rel="stylesheet">
</head>

<body class="bg-light">

	<div class="container">
		<div class="py-5 text-center">
			<p class="lead">
				<a href="index.jsp" class="btn btn-lg btn-secondary">Inicio</a>
			</p>
			<img class="d-block mx-auto mb-4"
				src="./resources/bootstrap/css/icon2.png" alt="" width="72"
				height="72">
			<h2>Fa�a seu pedido!</h2>
			<p class="lead">N�o fique com vontade, pe�a tudo o que quiser!</p>
		</div>

		<div class="row">
			<div class="col-md-4 order-md-2 mb-4"></div>
			<div class="col-md-12 order-md-1">
				<h4 class="mb-3">Seus Dados</h4>
				<form class="needs-validation" action="pedido" method="post">
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="firstName">Nome</label> <input type="text"
								class="form-control" id="nome" name="nome" placeholder="Nome">
							<div class="invalid-feedback">Precisamos de um primeiro
								nome v�lido :(</div>
						</div>
						<div class="col-md-6 mb-3">
							<label for="lastName">Sobrenome</label> <input type="text"
								class="form-control" name="sobrenome" id="sobrenome" placeholder="Sobrenome"
								value="">
							<div class="invalid-feedback">Precisamos de um sobrenome
								v�lido :(</div>
						</div>
					</div>

					<div class="mb-3">
						<label for="email">Email <span class="text-muted"></span></label>
						<input type="email" class="form-control" id="email" name="email"
							placeholder="seu@email.com.br">
						<div class="invalid-feedback">Entre com um email v�lido, por
							favor !</div>
					</div>

					<div class="mb-3">
						<label for="endereco">Endere�o Principal</label> <input type="text"
							class="form-control" id="endereco" name="endereco"
							placeholder="Avenida, rua, etc...">
						<div class="invalid-feedback">N�o quer receber a pizza??
							Coloca um endere�o que a gente ache :D</div>
					</div>

					<div class="row">
						<div class="col-md-5 mb-3">
							<label for="cidade">Cidade</label> <select
								class="custom-select d-block w-100" id="cidade" name="cidade">
								<option value="N�o escolhido">Escolha...</option>
								<option value="Santo Andr�">Santo Andr�</option>
								<option value="S�o Bernardo do Campo">S�o Bernardo do Campo</option>
								<option value="S�o Caetano do Sul">S�o Caetano do Sul</option>
							</select>
							<div class="invalid-feedback">Favor inserir uma cidade
								v�lida.</div>
						</div>
						<div class="col-md-4 mb-3">
							<label for="prf">Ponto de refer�ncia</label> <input type="text"
								class="form-control" id="prf" placeholder="Pr�ximo a..." name="pref">
							<div class="invalid-feedback">Ajuda o motoca, vai...</div>
						</div>
						<div class="col-md-3 mb-3">
							<label for="cep">CEP</label> <input type="text"
								class="form-control" id="cep" placeholder="XXXXX-XXX" name="cep">
							<div class="invalid-feedback">CEP nos ajudaria tanto...</div>
						</div>
					</div>
					<hr class="mb-4">

					<h4 class="mb-3">Pedido</h4>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="massa">Massa</label> <select
								class="custom-select d-block w-100" id="massa" name="massa">
								<option value="">Escolha...</option>
								<option value="Tradicional">Tradicional</option>
								<option value="Light">Light</option>
							</select>
						</div>
						<div class="col-md-6 mb-3">
							<label for="borda">Borda</label> <select
								class="custom-select d-block w-100" id="borda" name="borda">
								<option value="">Escolha...</option>
								<option value="Sem borda">Sem borda</option>
								<option value="Catupiry">Catupiry</option>
								<option value="Cheddar">Cheddar</option>
								<option value="Mussarela">Mussarela</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="sabor1">Sabor 1</label> <select
								class="custom-select d-block w-100" id="sabor1" name="sabor1">
								<option value="">Escolha...</option>
								<option value="Mussarela">Mussarela</option>
								<option value="Frango com catupiry">Frango com catupiry</option>
								<option value="Frango com cheddar">Frango com cheddar</option>
								<option value="Calabresa">Calabresa</option>
								<option value="Calabresa com catupiry">Calabresa com catupiry</option>
								<option value="Atum">Atum</option>
								<option value="Carne seca">Carne seca</option>
							</select>
						</div>
						<div class="col-md-6 mb-3">
							<label for="sabor2">Sabor 2<span class="text-muted">(Opcional)</span></label>
							<select class="custom-select d-block w-100" id="sabor2" name="sabor2">
								<option value="">Escolha...</option>
								<option value="Mussarela">Mussarela</option>
								<option value="Frango com catupiry">Frango com catupiry</option>
								<option value="Frango com cheddar">Frango com cheddar</option>
								<option value="Calabresa">Calabresa</option>
								<option value="Calabresa com catupiry">Calabresa com catupiry</option>
								<option value="Atum">Atum</option>
								<option value="Carne seca">Carne seca</option>
							</select>
						</div>
						<div class="col-md-6 mb-3">
							<label for="bebidas">Acompanha bebida?<span
								class="text-muted">(Opcional)</span></label> <select
								class="custom-select d-block w-100" id="bebidas" name="bebidas">
								<option value="">Escolha...</option>
								<option value="N�o quero">N�o quero</option>
								<option value="Coca-cola">Coca-cola</option>
								<option value="Fanta guaran�">Fanta guaran�</option>
								<option value="Fanta laranja">Fanta laranja</option>
								<option value="Fanta uva">Fanta uva</option>
								<option value="Pepsi">Pepsi</option>
								<option value="H20h">H20h</option>
								<option value="Vinho">Vinho</option>
							</select>
						</div>
						<div class="col-md-6 mb-3">
							<label for="doce">E que tal adocicar?<span
								class="text-muted">(Opcional)</span></label> <select
								class="custom-select d-block w-100" id="doce" name="doce">
								<option value="">Escolha...</option>
								<option value="Pizza romeu e julieta">Pizza romeu e julieta</option>
								<option value="Pizza banana com canela">Pizza banana com canela</option>
								<option value="Pizza de Brigadeiro">Pizza de Brigadeiro</option>
								<option value="Pizza prestigio">Pizza prestigio</option>
							</select>
						</div>
						<div class="col-md-12 mb-3">
							<label for="obs">Observa��es <span class="text-muted"></span></label>
							<input type="text" class="form-control" id="obs" name="obs"
								placeholder="Quer nos falar algo?">
						</div>
					</div>

					<hr class="mb-4">

					<h4 class="mb-3">Pagamento</h4>

					<div class="col-md-6 mb-3">
							<label for="pgmt">Forma de pagamento<span
								class="text-muted">(Opcional)</span></label> <select
								class="custom-select d-block w-100" id="pgmt" name="pgmt">
								<option value="">Escolha...</option>
								<option value="PIX">PIX</option>
								<option value="Cart�o de Cr�dito">Cart�o de Cr�dito</option>
								<option value="Cart�o de D�bito">Cart�o de D�bito</option>
								<option value="Dinheiro">Dinheiro</option>
							</select>
						</div>
					<hr class="mb-4">
					<button class="btn btn-primary btn-lg btn-block" type="submit">Pedir</button>
				</form>
			</div>
		</div>

		<footer class="my-5 pt-5 text-muted text-center text-small">
			<p class="mb-1">&copy; 2021</p>
			<ul class="list-inline">
				<li class="list-inline-item"><a>Privacy</a></li>
				<li class="list-inline-item"><a>Terms</a></li>
				<li class="list-inline-item"><a>Support</a></li>
			</ul>
		</footer>
	</div>

</body>
</html>