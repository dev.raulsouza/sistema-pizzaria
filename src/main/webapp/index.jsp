<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./resources/bootstrap/img/icon.png">

    <title>Souza's Pizzaria</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/cover/">

    <!-- Bootstrap core CSS -->
    <link href="./resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./resources/bootstrap/css/cover.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Souza's Pizzaria</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link active" href="#">Pgina principal</a>
            <a class="nav-link" href="cardapio.jsp">Cardpio</a>
            <a class="nav-link" href="fale.jsp">Fale conosco</a>
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
        <h1 class="cover-heading">PIZZARIA DO PROFESSOR RODRIGO</h1>
        <p class="lead">Acreditamos que todo processo deve ser atualizado e toda a��o evolu�da, por�m, nosso sabor deve carregar
        a experi�ncia para que em uma mordida o cliente saboreie a nossa evolu��o.</p>
        <p class="lead">
          <a href="pedido.jsp" class="btn btn-lg btn-secondary">Fa�a seu pedido!</a>
        </p>
      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Souza's Pizzaria - Avenida massa, esquina com Molho de tomate!</p>
        </div>
      </footer>
    </div>

  </body>
</html>
